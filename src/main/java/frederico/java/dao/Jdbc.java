package frederico.java.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author frederico
 */
public class Jdbc {

    private static final String DATA_SOURCE_NAME = "jdbc/DS";
    private static final Logger LOGGER = Logger.getLogger(Jdbc.class.getName());

    private static DataSource dataSource;

    private static DataSource getDataSource() throws ConnectionException {

        if (dataSource == null) {

            Context ctx = null;

            try {
                ctx = new InitialContext();
            } catch (NamingException ex) {
                String msg = "Aplicacao sem contexto.";
                LOGGER.severe(msg);
                throw new ConnectionException(msg, ex);
            }

            try {
                dataSource = (DataSource) ctx.lookup("java:/comp/env/" + DATA_SOURCE_NAME);
                LOGGER.info(String.format("Fonte de dados '%s' inicializada.", DATA_SOURCE_NAME));
            } catch (NamingException ex) {
                String msg = String.format("A fonte de dados '%s' nao foi localizada.", DATA_SOURCE_NAME);
                LOGGER.severe(msg);
                throw new ConnectionException(msg, ex);
            }

        }

        return dataSource;

    }

    public static Connection getConnection() throws ConnectionException {
        try {
            return getDataSource().getConnection();
        } catch (SQLException ex) {
            String msg = String.format("Erro ao conectar com o banco de dados: %s", ex.getMessage());
            LOGGER.severe(msg);
            throw new ConnectionException(msg, ex);
        }
    }

    public static void close(AutoCloseable obj) {
        if (obj != null) {
            try {
                obj.close();
            } catch (Exception ex) {
                // ignore
            }
        }
    }
}
