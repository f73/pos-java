package frederico.java.web.servlet;

import frederico.java.dao.Jdbc;
import frederico.java.utils.StringUtils;
import frederico.java.web.ConstantesWeb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author frederico
 */
@WebServlet(name = "GetServlet", urlPatterns = {"/get"})
public class GetServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder json = new StringBuilder();
        json.append("{");
        json.append("\"status\":").append("\"ok\"");
        json.append(",\"comentarios\":[");

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            con = Jdbc.getConnection();

            try {

                ps = con.prepareStatement("SELECT id, nome, texto, ts_criacao FROM comentario ORDER BY ts_criacao DESC LIMIT ?");
                ps.setInt(1, 10);
                rs = ps.executeQuery();

                String separador = "";
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                while (rs.next()) {

                    json.append(separador);
                    json.append("{");
                    json.append("\"id\":").append(rs.getInt("id"));
                    json.append(",").append("\"nome\":\"").append(StringUtils.unicodeEscape(rs.getString("nome"))).append("\"");
                    json.append(",").append("\"texto\":\"").append(StringUtils.unicodeEscape(rs.getString("texto"))).append("\"");
                    json.append(",").append("\"ts_criacao\":\"").append(dateFormat.format(new Date(rs.getTimestamp("ts_criacao").getTime()))).append("\"");
                    json.append("}");
                    separador = ",";

                }

            } finally {
                Jdbc.close(rs);
                Jdbc.close(ps);
            }

        } catch (Exception ex) {
            throw new ServletException(String.format("Erro: %s", ex.getMessage()), ex);
        } finally {
            Jdbc.close(con);
        }

        json.append("]"); // fim comentarios
        json.append("}"); // fim json

        response.setContentType(ConstantesWeb.CONTEN_TTYPE_JSON_UTF8);
        try (PrintWriter out = response.getWriter()) {
            out.print(json.toString());
        }

    }

}
