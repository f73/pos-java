package frederico.java.web.servlet;

import frederico.java.dao.Jdbc;
import frederico.java.utils.StringUtils;
import frederico.java.web.ConstantesWeb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author frederico
 */
@WebServlet(name = "PostServlet", urlPatterns = {"/post"})
public class PostServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder json = new StringBuilder();
        json.append("{");

        List<String> msgs = new ArrayList<>();

        String nome = request.getParameter("nome");
        if (nome == null || "".equals(nome.trim()) || nome.length() < 5 || nome.length() > 250) {
            msgs.add("Inválido: Nome. Informe entre 5 e 250 caracteres.");
        }

        String texto = request.getParameter("texto");
        if (texto == null || "".equals(texto.trim()) || texto.length() < 5 || texto.length() > 1000) {
            msgs.add("Inválido: Texto. Informe entre 5 e 1000 caracteres.");
        }

        if (msgs.isEmpty()) {

            Connection con = null;
            PreparedStatement ps = null;

            try {

                con = Jdbc.getConnection();

                try {

                    ps = con.prepareStatement("INSERT INTO comentario (nome, texto) VALUES (?, ?)");
                    ps.setString(1, nome);
                    ps.setString(2, texto);
                    ps.executeUpdate();

                    json.append("\"status\":\"ok\"");

                } finally {
                    Jdbc.close(ps);
                }

            } catch (Exception ex) {
                throw new ServletException(String.format("Erro: %s", ex.getMessage()), ex);
            } finally {
                Jdbc.close(con);
            }

        } else {

            json.append("\"status\":\"erro_validacao\"");

            json.append(",\"msgs\":[");
            if (!msgs.isEmpty()) {
                String separador = "";
                for (String msg : msgs) {
                    json.append(separador);
                    json.append("\"").append(StringUtils.unicodeEscape(msg)).append("\"");
                    separador = ",";
                }
            }
            json.append("]"); // fim msgs

        }

        json.append("}"); // fim json

        response.setContentType(ConstantesWeb.CONTEN_TTYPE_JSON_UTF8);
        try (PrintWriter out = response.getWriter()) {
            out.print(json.toString());
        }

    }

}
